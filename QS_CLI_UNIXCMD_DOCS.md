#Qingstor CLI UnixCMD
--------------

Qingstor CLI UnixCMD provides more powerful unix-like commands. You can manage Qingstor resources just like files on local machine. UnixCMD contains: cp, ls, mk-bucket, mv, rm, rm-bucket and sync. All of the them support batch processing.

###Brief Introduction

        cp - Copy local file(s) to qs object or qs object(s) to local.
          local-path qs-uri or qs-uri local-path
          --recursive
          --include <value>  
          --exclude <value>

        ls - List qs objects and common prefixes under a prefix or all qs buckets.
          qs-uri or NONE
          --recursive
          --page-size <value>

        mk-bucket - Create an qs bucket.
          qs-uri
          --region <value>
          
        mv - Move local file(s) to qs or qs object(s) to local.
          local-path qs-uri or qs-uri local-path
          --recursive
          --include <value>  
          --exclude <value>          

        rm-bucket - Delete an empty qs bucket or forcely delete nonempty qs bucket
          qs-uri
          --force

        rm - Delete a qs object or more.
          qs-uri
          --recursive   
          --page-size <value>
          --include <value>  
          --exclude <value>

        sync - sync directories and qs prefixes.
          local-path qs-uri or qs-uri local-path
          --delete
          --page-size <value>
          --include <value>  
          --exclude <value>

Detailed usages and examples are listed below.

#Usages and Examples
--------------          

##cp

    Command
       cp

    SYNOPSIS

            cp
          local-path qs-uri or qs-uri local-path
          --include <value>
          --exclude <value>
          --recursive (boolean)

    OPTIONS

       --include  (string)  Don't exclude files or objects in the command that
       match the specified pattern.
       
       --exclude  (string)  Exclude all files or objects from the command that
       matches the specified pattern.
       
       --recursive (boolean)

    EXAMPLES

       1, Copying a local file to qs

       The following cp command copies a single file to a specified bucket and
       key:

          qsctl cp test.txt qs://mybucket/test2.txt

       Output:

          upload: test.txt to qs://mybucket/test2.txt

       2, Recursively copying qs objects to a local directory

       When passed with the parameter --recursive, the  following  cp  command
       recursively copies all objects under a specified prefix and bucket to a
       specified directory.  In this example,  the  bucket  mybucket  has  the
       objects test1.txt and test2.txt:

          qsctl cp qs://mybucket . --recursive

       Output:

          download: qs://mybucket/test1.txt to test1.txt
          download: qs://mybucket/test2.txt to test2.txt
          
       3, Recursively copying local files to bucket with --exclude and --inclu
       -de options

       When  passed  with  the parameter --recursive, the following cp command
       recursively copies all files  under  a specifed directory to a specifed
       bucket  while  excluding  some objects by using an --exclude parameter.
       In this example, the bucket mybucket  has  the  objects  test1.txt  and
       another/test1.txt:

          qsctl cp myDir qs://mybucket/ --recursive --exclude "myDir/another/*"

       Output:

          copy: myDir/test1.txt to qs://mybucket/test1.txt

       You  can  combine  --exclude and --include options to copy only objects
       that match a pattern, excluding all others:

          qsctl cp myDir/logs/ qs://mybucket/logs/ --recursive --exclude "*" 
          --include "*.log"

       Output:

          copy: mydir/test/test.log to qs://mybucket/test/test.log
          copy: myDir/test3.log to qs://mybucket/test3.log

##ls

    Command
       ls
       
    SYNOPSIS
            ls
          qs-uri or NONE
          --recursive
          --page-size <value>

    OPTIONS
       paths (string)

       --recursive (boolean) Command is performed  on  all  files  or  objects
       under the specified directory or prefix.

       --page-size  (integer) The number of results to return in each response
       to a list operation. The default value is 1000 (the  maximum  allowed).
       Using a lower value may help if an operation times out.

    EXAMPLES

       1, List all buckets
       
       The following ls command lists all of the bucket owned by the user.  In
       this example, the user owns the buckets mybucket  and  mybucket2.   The
       CreationTime is the date the bucket was created.  Note if qs:// is used
       for the path argument qs-uri, it will list all of the buckets as well:

          qsctl ls

       Output:

          2013-07-11 17:08:50 mybucket
          2013-07-24 14:55:44 mybucket2

       2, List objects
       
       The following ls command lists objects  and  common  prefixes  under  a
       specified bucket and prefix.  In this example, the user owns the bucket
       mybucket with the objects test.txt and somePrefix/test.txt.  The  Last-
       WriteTime  and Length are arbitrary. Note that since the ls command has
       no interaction with the local filesystem, the qs:// URI scheme  is  not
       required to resolve ambiguity and may be ommited:

          qsctl ls qs://mybucket

       Output:

                                     PRE somePrefix/
          2013-07-25 17:06:27         88 test.txt

       3, List objects with prefixes
       
       The  following  ls  command  lists  objects and common  prefixes under a
       specified bucket and prefix.  In this example, there are  no objects nor
       common prefixes under the specified bucket and prefix:

          qsctl ls qs://mybucket/noExistPrefix

       Output:

          None

       4, Recursively list objects
       
       The  following  ls  command  will recursively list objects in a bucket.
       Rather than showing PRE dirname/ in the output, all the  content  in  a
       bucket will be listed in order:

          qsctl ls qs://mybucket --recursive

       Output:

          2013-09-02 21:37:53         10 a.txt
          2013-09-02 21:37:53    2863288 foo.zip
          2013-09-02 21:32:57         23 foo/bar/.baz/a
          2013-09-02 21:32:58         41 foo/bar/.baz/b
          2013-09-02 21:32:57        281 foo/bar/.baz/c
          2013-09-02 21:32:57         73 foo/bar/.baz/d
          2013-09-02 21:32:57        452 foo/bar/.baz/e
          2013-09-02 21:32:57        896 foo/bar/.baz/hooks/bar
          2013-09-02 21:32:57        189 foo/bar/.baz/hooks/foo
          2013-09-02 21:32:57        398 z.txt

##mk-bucket

    Command
       mb

    SYNOPSIS
            mb
           qs-uri

    OPTIONS
       --region <value>

    EXAMPLES
       The following mk-bucket command creates a bucket.  In this example, the
       user makes the bucket mybucket. The bucket is created in the region spe
       -cified in the user's configuration file:

          qsctl qs mk-bucket qs://mybucket

       Output:

          make_bucket: mybucket

       The following mk-bucket command creates a bucket  in a region specified
       by the --region parameter. In this example, the user makes  the  bucket
       mybucket in the region pek3a:

          qsctl qs mb qs://mybucket --region pek3a

       Output:

          make_bucket: mybucket

##mv

    Command
       mv -

    SYNOPSIS
            mv
          local-path qs-uri or qs-uri local-path
          --include <value>
          --exclude <value>
          --recursive

    OPTIONS
       --include <value>
       --exclude <value>
       --recursive (boolean)

    EXAMPLES

       1, Move a single file to a specified bucket and key
       
       The following mv command moves a single file to a specified bucket and
       key:

          qsctl mv test.txt qs://mybucket/test2.txt

       Output:

          move: test.txt to qs://mybucket/test2.txt

       2, Move a single object to a specified file locally
       
       The following mv command moves a single object to a specified file
       locally:

          qsctl mv qs://mybucket/test.txt test2.txt

       Output:

          move: qs://mybucket/test.txt to test2.txt

       3, Recursively move all objects under a specified prefix and bucket to a
       specified directory
       
       When passed with the parameter --recursive, the  following  mv  command
       recursively  moves all objects under a specified prefix and bucket to a
       specified directory.  In this example,  the  bucket  mybucket  has  the
       objects test1.txt and test2.txt:

          qsctl mv qs://mybucket . --recursive

       Output:

          move: qs://mybucket/test1.txt to test1.txt
          move: qs://mybucket/test2.txt to test2.txt
          
       4, Recursively move with --exclude and --include options

       When  passed  with  the parameter --recursive, the following mv command
       recursively moves all files under a specifed directory to  a  specified
       bucket  and  prefix  while  excluding  some files by using an --exclude
       parameter. In this example, the directory myDir has the files test1.txt
       and test2.jpg:

          qsctl mv myDir qs://mybucket/ --recursive --exclude "*.jpg"

       Output:

          move: myDir/test1.txt to qs://mybucket/test1.txt

       You  can  combine  --exclude and --include options to move only objects
       that match a pattern, excluding all others:

          qsctl mv myDir/logs/ qs://mybucket/logs/ --recursive --exclude "*" 
          --include "*.log"

       Output:

          move: mydir/test/test.log to qs://mybucket/test/test.log
          move: myDir/test3.log to qs://mybucket/test3.log

##rm

    Command
       rm

    SYNOPSIS
            rm
          qs-uri
          --recursive
          --page-size <value>

    OPTIONS
    
       --include (string) Don't exclude files or objects in the  command  that
       match the specified pattern. 

       --exclude (string) Exclude all files or objects from the  command  that
       matches the specified pattern.

       --recursive (boolean)  Command  is  performed  on all files or objects
       under the specified directory or prefix.

       --page-size (integer) The number of results to return in each  response
       to  a  list operation. The default value is 1000  (the maximum allowed).
       Using a lower value may help if an operation times out.

    EXAMPLES
    
       1, Delete a single qs object
       
       The following rm command deletes a single qs object:

          qsctl rm qs://mybucket/test2.txt

       Output:

          delete: qs://mybucket/test2.txt

       2, Recursively delete all objects
       
       The following rm command recursively deletes all objects under a speci-
       fied  bucket and prefix when passed with the parameter --recursive.  In
       this example, the bucket mybucket contains the  objects  test1.txt  and
       test2.txt:

          qsctl rm qs://mybucket --recursive

       Output:

          delete: qs://mybucket/test1.txt
          delete: qs://mybucket/test2.txt

       3, Recursively delete objects with --exclude and --include options
       
       The following rm command recursively deletes all objects under a speci-
       fied bucket and prefix when passed with the parameter --recursive while
       excluding  some objects by using an --exclude parameter.  In this exam-
       ple, the bucket mybucket has the objects test1.txt and test2.jpg:

          qsctl rm qs://mybucket/ --recursive --exclude "*.jpg"

       Output:

          delete: qs://mybucket/test1.txt

       You  can  combine  --exclude and --include options to move only objects
       that match a pattern, excluding all others:

          qsctl rm qs://mybucket/logs/ --recursive --exclude "*" --include 
          "*.log"

       Output:

          delete: qs://mybucket/test/test.log
          delete: qs://mybucket/test3.log


##rm-bucket

    NAME
       rm-bucket

    DESCRIPTION
       Deletes  an  empty  qs  bucket.  A  bucket  must be completely empty of
       objects before it can be deleted. However, the --force parameter can be
       used to delete the nonempty bucket.

    SYNOPSIS
            rm-bucket
          qs-uri
          --force

    OPTIONS
    
       --force (boolean) Deletes all  objects  in  the  bucket  including  the
       bucket  itself. 

    EXAMPLES
       The following rb command removes a bucket.  In this example, the user's
       bucket  is  mybucket.   Note  that the bucket must be empty in order to
       remove:

          qsctl qs rb qs://mybucket

       Output:

          remove_bucket: mybucket

       The following rb command uses the --force parameter to first remove all
       of  the  objects  in  the bucket and then remove the bucket itself.  In
       this example, the user's bucket is mybucket and the objects in mybucket
       are test1.txt and test2.txt:

          qsctl qs rb qs://mybucket --force

       Output:

          delete: qs://mybucket/test1.txt
          delete: qs://mybucket/test2.txt
          remove_bucket: mybucket

##sync

    Command
       sync

    SYNOPSIS
            sync
          local-path qs-uri or qs-uri local-path
          [--delete]
          [--page-size <value>] 

    EXAMPLES
    
       1, Sync objects under a specified prefix and bucket to files in a local
       directory
       
       The following  sync command syncs objects under a  specified prefix and
       bucket to files in a  local directory  by uploading  the local files to
       qs.  A local file will  require uploading if the size of the local file
       is different than the size of the qs object, the last modified time  of
       the local file is newer than the last modified time  of the  qs object,
       or the local file does not exist under the specified bucket and prefix.
       In this example, the user syncs the bucket mybucket  to the  local cur-
       rent directory. The local current directory contains the files test.txt
       and test2.txt. The bucket mybucket contains no objects:

          qsctl sync . qs://mybucket

       Output:

          upload: test.txt to qs://mybucket/test.txt
          upload: test2.txt to qs://mybucket/test2.txt

       2, Sync files in a local directory to objects under  a specified prefix
       and bucket
       
       The following sync command syncs files in a local directory to  objects
       under  a  specified  prefix and bucket by downloading qs objects.  A qs
       object will require downloading if the size of the  qs  object  differs
       from  the  size  of  the  local  file, the last modified time of the qs
       object is older than the last modified time of the local file,  or  the
       qs  object  does not exist in the local directory.  Take note that when
       objects are downloaded from qs, the last modified  time  of  the  local
       file  is  changed  to the last modified time of the qs object.  In this
       example, the user syncs the  current  local  directory  to  the  bucket
       mybucket.   The  bucket  mybucket  contains  the  objects  test.txt and
       test2.txt.  The current local directory has no files:

          qsctl sync qs://mybucket .

       Output:

          download: qs://mybucket/test.txt to test.txt
          download: qs://mybucket/test2.txt to test2.txt

       3, --delete parameter
       
       The following sync command syncs objects under a specified  prefix  and
       bucket  to  files  in a local directory by uploading the local files to
       qs.  Because the --delete parameter flag is thrown, any files  existing
       under  the  specified  prefix  and bucket but not existing in the local
       directory will be deleted.  In this example, the user syncs the  bucket
       mybucket  to  the local current directory.  The local current directory
       contains the files test.txt and test2.txt.  The  bucket  mybucket  con-
       tains the object test3.txt:

          qsctl sync . qs://mybucket --delete

       Output:

          upload: test.txt to qs://mybucket/test.txt
          upload: test2.txt to qs://mybucket/test2.txt
          delete: qs://mybucket/test3.txt
          
       4, sync with --exclude and --include opition

       The  following  sync command syncs objects under a specified prefix and
       bucket to files in a local directory by uploading the  local  files  to
       qs.  Because the --exclude parameter flag is thrown, all files matching
       the pattern existing both in qs and locally will be excluded  from  the
       sync.  In this example, the user syncs the bucket mybucket to the local
       current directory.  The local  current  directory  contains  the  files
       test.jpg  and  test2.txt.   The  bucket  mybucket  contains  the object
       test.jpg of a different size than the local test.jpg:

          qsctl sync . qs://mybucket --exclude "*.jpg"

       Output:

          upload: test2.txt to s3://mybucket/test2.txt
          
       You  can  combine  --exclude and --include options to sync only objects
       that match a pattern, excluding all others:

          qsctl sync myDir/logs/ qs://mybucket/logs/ --recursive --exclude "*"
          --include "*.log"

       Output:

          upload: mydir/test/test.log to qs://mybucket/test/test.log
          upload: myDir/test3.log to qs://mybucket/test3.log
