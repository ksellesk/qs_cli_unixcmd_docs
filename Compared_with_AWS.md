#Compared with AWS
--------------

We support fewer features compared with AWS(see below). Is it nessesary to support some of these?

### aws

        cp - 
          <S3Uri> <S3Uri>    - bucket to bucket

        mv - 
          <S3Uri> <S3Uri>    - bucket to bucket

        sync - 
          <S3Uri> <S3Uri>    - bucket to bucket
          
        website - set the website configuration for a bucket.
          <S3Uri>
          [--index-document <value>]    - index.html
          [--error-document <value>]    - 4xx.html
          
        website example
          The following command configures a bucket named my-bucket as  a  static
          website:

             aws s3 website s3://my-bucket/ --index-document index.html --error-document
             error.html
 
          The index document option specifies the file in my-bucket that visitors
          will be directed to when they navigate to  the  website  URL.  In  this
          case,  the  bucket is in the us-west-2 region, so the site would appear
          at http://my-bucket.s3-website-us-west-2.amazonaws.com.

          All files in the bucket that appear on the static site must be  config-
          ured  to  allow  visitors to open them. File permissions are configured
          separately from the bucket website configuration.  For  information  on
          hosting  a static website in Amazon S3, see Hosting a Static Website in
          the Amazon Simple Storage Service Developer Guide.